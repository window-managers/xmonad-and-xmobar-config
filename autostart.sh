#!/usr/bin/env bash

function run {
    if ! pgrep -f $1 ;
      then
          $@&
            fi
}

run stalonetray &
run nm-applet 
run pasystray 
run redshift-gtk 
run udiskie 
run unclutter &
run ~/.fehbg &
run ulauncher --hide-window 
run /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 
run /usr/lib/geoclue-2.0/demos/agent 
run xrdb ~/.Xresources 
