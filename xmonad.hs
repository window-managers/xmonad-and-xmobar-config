--------------------------------------------------------------------------------
-- | Example.hs
--
-- Example configuration file for xmonad using the latest recommended
-- features (e.g., 'desktopConfig').
module Main (main) where

--------------------------------------------------------------------------------
-- Base
import XMonad
import System.IO
import System.Exit

-- Actions
import XMonad.Actions.CopyWindow

-- Config
import XMonad.Config.Desktop

-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.InsertPosition

-- Layout
import XMonad.Layout.BinarySpacePartition (emptyBSP)
import XMonad.Layout.Combo
import XMonad.Layout.Circle
import XMonad.Layout.Decoration
import XMonad.Layout.Fullscreen
import XMonad.Layout.Grid
import XMonad.Layout.Maximize
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.ResizableTile
import XMonad.Layout.ResizableTile (ResizableTall(..))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Layout.ToggleLayouts (ToggleLayout(..), toggleLayouts)
import XMonad.Layout.TwoPane
import XMonad.Layout.WindowNavigation

-- Prompt
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.RunOrRaise
import XMonad.Prompt.Shell

import XMonad.StackSet

-- Util
import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.Run

import Control.Arrow ((&&&),first)
import Data.List (partition)

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- Use Win or ALt key
myModMask = mod4Mask

-- Focus Mouse
myFocusFollowsMouse = True

-- Border Width
myBorderWidth = 1

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal = "alacritty"

-- Browser
myBrowser = "flatpak run org.chromium.Chromium"

-- File Manager
myFileManager = "nemo"

-- Run Prompt
myRunPrompt = "dmenu_run"

------------------------------------------------------------------------
-- Workspaces
-- The default number of workspaces (virtual screens) and their names.
--
myWorkspaces = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
--myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
myKeys :: [(String, X ())]
myKeys =

    --Xmonad
    [ ("M-S-q", io (exitWith ExitSuccess)) --Quit Xmonad
    , ("M-C-r", spawn "xmonad --restart") --Restart Xmonad 

    -- launch a terminal
    , ("M-<Return>", spawn myTerminal)
    
    -- launch a run prompt
    , ("M-d", spawn myRunPrompt)

    -- launch file manager
    , ("M-e", spawn myFileManager)

    -- launch web browser
    , ("M-b", spawn myBrowser) 

    -- launch shell prompt menu
    , ("M-p", shellPrompt myXPConfig)

    -- runOrRaisePrompt
    , ("M-S-r", runOrRaisePrompt myXPConfig)

    -- close focused window
    , ("M-S-c", kill)

     -- Rotate forward through the available layout algorithms
    , ("M-<Space>", sendMessage NextLayout)

    -- Move focus to the next window
    , ("M-<Tab>", windows W.focusDown)

    -- Move focus to the next window
    , ("M-j", windows W.focusDown)

    -- Move focus to the previous window
    , ("M-k", windows W.focusUp  )

    -- Move focus to the master window
    , ("M-m", windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ("M-S-s", windows W.swapMaster)

    -- Swap the focused window with the next window
    , ("M-S-j", windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ("M-S-k", windows W.swapUp    )

    -- Shrink the master area
    , ("M-h", sendMessage Shrink)

    -- Expand the master area
    , ("M-l", sendMessage Expand)

    -- Push window back into tiling
    , ("M-t", withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ("M-,", sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ("M-.", sendMessage (IncMasterN (-1)))
    
    -- toggle the status bar gap
    -- TODO, update this binding with avoidStruts
    , ("M-f", sendMessage ToggleStruts) --toggle fullscreen

    -- Powermenu
    , ("M-C-m", spawn "powermenu")

    -- Volume control
    , ("<XF86AudioMute>", spawn "vol-notify -t")

    , ("<XF86AudioLowerVolume>", spawn "vol-notify -d")

    , ("<XF86AudioRaiseVolume>", spawn "vol-notify -i")
    ]

------------------------------------------------------------------------
-- main function.
--
main = do

  -- Xmobar
  xmobar <- spawnPipe "xmobar ~/.config/xmobar/xmobarrc" -- Start a task bar such as xmobar.

  ------------------------------------------------------------------------
  -- Autostart
  -- Perform an arbitrary action each time xmonad starts or is restarted
  -- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
  -- per-workspace layout choices.
  --
  -- By default, do nothing.

  spawn "~/.xmonad/autostart.sh"

  
  -- Start xmonad using the main desktop configuration with a few
  -- simple overrides:
  xmonad $ fullscreenSupport $ desktopConfig
    { modMask = myModMask 
    , terminal = myTerminal
    , borderWidth = myBorderWidth
    , focusFollowsMouse = myFocusFollowsMouse
    , XMonad.workspaces = myWorkspaces
    , manageHook = insertPosition Below Newer <+> myManageHook <+> manageHook desktopConfig --spawn windows below previous windows
    , layoutHook = desktopLayoutModifiers myLayouts
    , logHook = dynamicLogWithPP $ def 
          { ppOutput = hPutStrLn xmobar,
            ppCurrent = xmobarColor "white" "#503677" . wrap "[" "]", --focused workspace
            ppHidden = xmobarColor "gray" "" . wrap "*" "", --hidden workspaces with windows
            ppHiddenNoWindows = xmobarColor "gray" "", --empty hidden workspaces
            ppTitle = xmobarColor "white" "" --window title 
          }
    }

    `additionalKeysP` myKeys -- Add key bindings

--------------------------------------------------------------------------------
-- | Customize layouts.
--
-- This layout configuration uses two primary layouts, 'ResizableTall'
-- and 'BinarySpacePartition'.  You can also use the 'M-<Esc>' key
-- binding defined above to toggle between the current layout and a
-- full screen layout.
--myLayouts = toggleLayouts (noBorders Full) others
myLayouts = tiled ||| Mirror tiled ||| grid ||| simpleTabbed ||| Full
  where
    --others = ResizableTall 1 (1.5/100) (3/5) [] ||| emptyBSP
    tiled = spacing 8 $ Tall nmaster delta ratio
    grid = spacing 8 $ Grid
    nmaster = 1
    ratio = 1/2
    delta = 3/100

--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
myXPConfig = def
  { position          = Top
  , alwaysHighlight   = True
  , promptBorderWidth = 0
  , height            = 24
  , font              = "xft:monospace:size=16"
  }

--------------------------------------------------------------------------------
-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.
--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook = composeOne
  -- Handle floating windows:
  [ transience            -- move transient windows to their parent
  , isDialog              -?> doCenterFloat
  ] <+> composeAll
  [ className =? "Pidgin" --> doFloat
  , className =? "XCalc"  --> doFloat
  , className =? "mpv"    --> doFloat
  ]

