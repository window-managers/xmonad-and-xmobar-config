#!/bin/sh

m=$(amixer sget -D pulse Master | awk -F"[][]" '/Left:/ { print $4 }')

#Unmute
if [ "$m" = "on" ]
then
	amixer sget -D pulse Master | awk -F"[][]" '/Left:/ { print $2 }'
#Mute
else
	echo "Mute"
fi
