# xmonad-and-xmobar-config
Configuration files for xmonad and xmobar.

## Screenshot
![](/screenshot/xmonad.png)

## xmonad config location
```
~/.xmonad/xmonad.hs
```
## xmobar config location
```
~/.config/xmobar/xmobarrc
```
## autostart script location
```
~/.xmonad/autostart.sh
```
## Remember to make autostart.sh executable
```
chmod +x ~/.xmonad/autostart.sh
```

## Programs:
### Install these programs from your linux distribution package manager.
  1. alacritty or terminal emulator of your choice 
  2. dmenu 
  3. nemo 
  4. chromium/firefox 
  5. redshift
  6. feh 
  7. network-manager-applet  
  8. udiskie 
  9. polkit-gnome/policykit-1-gnome 
  10. geoclue2 
  11. pulseaudio 
  12. unclutter 
  13. discord 
  14. stalonetray 
  15. xdotool 
  16. vol-notify 
  17. powermenu 

## stalonetray-config
repo: https://gitlab.com/window-managers/stalonetray-config

## vol-notify
repo: https://gitlab.com/window-managers/vol-notify

## powermenu
repo: https://gitlab.com/window-managers/powermenu

## Credit to the creators of xmonad
Official Website: https://xmonad.org/

Repository: https://github.com/xmonad/xmonad

## Credit to the creators of xmobar
Official Website: https://xmobar.org/

Repository: https://github.com/jaor/xmobar

## Wallpaper
### minimal mystic mountains
https://www.reddit.com/r/wallpapers/comments/hq8d53/minimalistic_mystical_mountains_1920_x_1080/?utm_medium=android_app&utm_source=share
